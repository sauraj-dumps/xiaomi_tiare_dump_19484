## tiare-userdebug 8.1.0 OPM1.171019.026 FACTORY test-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: tiare
- Brand: Xiaomi
- Flavor: tiare-userdebug
- Release Version: 8.1.0
- Id: OPM1.171019.026
- Incremental: FACTORY
- Tags: test-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/tiare/tiare:8.1.0/OPM1.171019.026/FACTORY:userdebug/test-keys
- OTA version: 
- Branch: tiare-userdebug-8.1.0-OPM1.171019.026-FACTORY-test-keys
- Repo: xiaomi_tiare_dump_19484


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
